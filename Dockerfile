FROM debian:bookworm-slim

LABEL maintainer="Greg Myers" \
      description="GitLab CLI (glab) in a container image, with additional tools" \
      version="1.0.0" \
      org.opencontainers.image.source="https://registry.gitlab.com/greg/glab"

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install --no-install-recommends -y \
    curl \
    ca-certificates \
    jq \
    git && \
    latest=$(curl -sL https://gitlab.com/api/v4/projects/34675721/releases/permalink/latest | grep --only-matching 'v[0-9\.]\+' | cut -c 2- | head -n 1) && \
    curl -sL -o /tmp/glab.deb "https://gitlab.com/gitlab-org/cli/-/releases/v${latest}/downloads/glab_${latest}_linux_amd64.deb" && \
    apt install -y /tmp/glab.deb && \
    rm /tmp/glab.deb && \
    apt-get -y clean && \
    rm -rf /var/lib/apt/lists/* && \
    glab config -g set check_update false

CMD ["glab"]
