# glab

[![pipeline status](https://gitlab.com/greg/glab/badges/main/pipeline.svg)](https://gitlab.com/greg/glab/-/commits/main)

Dockerized [GitLab CLI (glab)](https://gitlab.com/gitlab-org/cli) with additional tools - optimized for GitLab CI/CD.

## Features

- Official `glab` CLI pre-installed
- Lightweight Debian base image
- Includes `curl`, `jq`, and `git`
- CI/CD optimized

## Usage

```bash
docker run --rm -it registry.gitlab.com/greg/docker-glab bash
```

## CI/CD

```yaml
job:
  image: "registry.gitlab.com/greg/glab"
  # script:
  # - glab --help
```
